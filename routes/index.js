
/*
 * GET home page.
 */

exports.index = function(req, res){
  req.app.set('room', req.query.room);
  res.render('index', { title: 'Express' , room: req.query.room});
  
  
};