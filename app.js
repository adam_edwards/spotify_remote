
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , https = require('https')
  , url = require('url')
  , path = require('path')
  , socket = require('socket.io');
 

var app = express();
  server = http.createServer(app);
    io = socket.listen(server);
// all environments
app.set('port', process.env.PORT || 1024);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
 
});

io.sockets.on('connection', function (socket) {
    
  socket.on('player-connect', function (data) {
       var longUrl =   'http://spotify.adamledwards.com?room='+data.identifier;
    makebitly(longUrl,socket);
        socket.join(data.identifier);
       
      //  socket.emit('ready', {url: longUrl});
        
  });
  
  
});




io.sockets.authorization(function(handshakeData, callback){
    console.log(io.sockets.manager.rooms); 
    app.get('room');
        
     callback(null,true);
}).on('connection', function (socket) {
    socket.on('joinRoom', function(data){
    
        socket.join(data);
    });
    
    socket.on('remote-command', function(data){
        io.sockets.in(data.room).emit('command',{action:data.command, data:data.meta});
    });
    
        
    socket.on('remote-update', function(data){
        io.sockets.in(data.room).emit('update',data.player);
    });
});
//  socket.on('remote-connect', function (data) {  });





function makebitly(longurl,socket){
      endpoint = '/v3/shorten?access_token=68428816d8286326c68d344edc7c0cd4d3e0becd&longurl='+longurl;
      
            var options = {
        host: 'api-ssl.bitly.com',
        path: endpoint,
        method: 'GET'
      };

      var req = https.request(options, function(res) {

       // console.log('STATUS: ' + res.statusCode);
        //console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
          body= JSON.parse(chunk);
        
        });
        
         res.on('end', function () {
        console.log(body.data.url);
          socket.emit('ready', {url: body.data.url});
        
        });
      });

      req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
      });
      req.end();
  
      
  }