// Some general UI pack related JS
// Extend JS String with repeat method
String.prototype.repeat = function(num) {
    return new Array(num + 1).join(this);
};

(function($) {

  // Add segments to a slider
  $.fn.addSliderSegments = function (amount) {
    return this.each(function () {
      var segmentGap = 100 / (amount) + "%"
        , segment = "<div class='ui-slider-segment' style='margin-left: " + segmentGap + ";'></div>";
      $(this).prepend(segment.repeat(amount - 1));
    });
  };

  $(function() {
  
    // Todo list
    $(".todo li").click(function() {
        $(this).toggleClass("todo-done");
    });

    // Custom Select
    $("select[name='herolist']").selectpicker({style: 'btn-primary', menuStyle: 'dropdown-inverse'});

    // Tooltips
    $("[data-toggle=tooltip]").tooltip("show");

    // Tags Input
    $(".tagsinput").tagsInput();

    // jQuery UI Sliders
    var $slider = $("#slider");
    if ($slider.length) {
      $slider.slider({
        min: 0,
        max: 10,
        value: 2,
        orientation: "horizontal",
        range: "min"
      }).addSliderSegments($slider.slider("option").max);
    }

    // Placeholders for input/textarea
    $("input, textarea").placeholder();

    // Make pagination demo work
    $(".pagination a").on('click', function() {
      $(this).parent().siblings("li").removeClass("active").end().addClass("active");
    });

    $(".btn-group a").on('click', function() {
      $(this).siblings().removeClass("active").end().addClass("active");c
    });

    // Disable link clicks to prevent page scrolling
    $('a[href="#fakelink"]').on('click', function (e) {
      e.preventDefault();
    });

    // Switch
    $("[name='repeat']").wrap('<div id="repeat" class="switch" />').parent().bootstrapSwitch();
    $("[name='shuffle']").wrap('<div id="shuffle" class="switch" />').parent().bootstrapSwitch();
    
  });
  
  
  $('document').ready(function(){
      window.scrollTo(0,1);
       $.get("views/searchResult.html").done(function(data){
          view = data; 
       });
      prodUrl = 'http://spotify.adamledwards.com'; 
       devUrl = 'http://localhost:1024'; 
     socket = io.connect(prodUrl);
      socket.on('connect', function (data) {
          socket.emit('joinRoom', room);
      });
      
      socket.on('update', function (data) {
 
         trackProgress(data.position,data.duration,data.playing);
         $('#song').html(data.track.artist+' - '+data.track.name);
       
         if(data.playing){
             
             $("[data-action='playPause']").addClass('fui-pause').removeClass('fui-play');
         }else{
             $("[data-action='playPause']").removeClass('fui-pause').addClass('fui-play');
         }
  
         $("#slider").off("slidestop");
         $("#slider").slider( "value", data.volume*10 );
         $("#slider").on( "slidestop", function( event, ui ) {
            setVolume(ui.value);
         } );
         //$("#repeat").bootstrapSwitch('setState',data.repeat);
          //$("#shuffle").bootstrapSwitch('setState',data.shuffle);
    
      });
      bindEvents();
      ;
  });
  

  
})(jQuery);

var id= '';  
function trackProgress(position, duration,playing){
  
 
  if(id!==''){
   
      clearInterval(id);
  }
 
   if(playing){

    id = setInterval(function(){

         position+= 1000;

           progMade =  position/duration*100;
           $('#seek').css('width',progMade+'%');
     },1000);
    }
   progMade =  position/duration*100;
   if(duration==0){
       progMade= 0;
       //request duration again
   }
 // console.log(progMade);
  $('#seek').css('width',progMade+'%');
  
}
   
function bindEvents(){
  

  $("[data-action]").bind("click", function(e){
          
          action =$(this).data('action');
          
          switch(action){
              case 'next':
              case 'prev':
              case 'playPause':    
                   socket.emit('remote-command', {command: action, room:room, meta:"" });
                  break;
          }
          
          
      });
      
  $("[data-toggle='switch']").bind('change',function(e){
          onOff =  $(this).is(':checked')?1:0;
          name = $(this).attr('name');
         
          socket.emit('remote-command', {command:name, room:room, meta:onOff });
  });
  
  $('#trackSearch').on('click','ul li', function(){
     href =$(this).data('href'); 
     console.log(href);
     socket.emit('remote-command', {command: 'track', room:room, meta:href });
     
  });
  var  prevSTerm;
  $('a[data-toggle="tab"]').on('shown', function (e) {
      sTerm =document.forms['searchForm']['search'].value;
      if(sTerm!=''){
        console.log(e.relatedTarget.href); 
        
        if ( _.isUndefined(prevSTerm) ){
            prevSTerm = '';
        };
        console.log(prevSTerm);
      
        sTerm = document.forms['searchForm']['search'].value;
        
        console.log(e);
        
        prevSTerm = sTerm;
      }
});
  
};

$('#searchForm').submit(function(e){
  id = $('.tab-content .active').attr('id');
console.log(id);
getSearchResults(id);
  return false;
});

function setVolume(value){
    socket.emit('remote-command', {command:'volume', room:room, meta:value/10 });
  
};
function getSearchResults(searchFor){
    switch(searchFor){
        case 'trackSearch':
            url ='http://ws.spotify.com/search/1/track.json';
            break;
        case 'artistSearch':
            url ='http://ws.spotify.com/search/1/artist.json';
            break;
        case 'albumSearch':
             url ='http://ws.spotify.com/search/1/album.json';
             break;
             
    }
    console.log(url);
 sTerm = document.forms['searchForm']['search'].value;
  $.ajax({
      url : url,
      data: {q:sTerm}
      
  }).done(function(response){
      switch(searchFor){
        case 'trackSearch':
            data = response.tracks;
            break;
        case 'artistSearch':
            data = response.artists;
            break;
        case 'albumSearch':
             data = response.albums;
             break;
             
    };
    
      
    renderView =  _.template(view, {data:data , info:response.info});
 
  $('#'+response.info.type+'Search').html(renderView);
  $('#'+response.info.type+'Search').attr('data-sterm',sTerm);
      console.log($('#'+response.info.type+'Search'));
  });

}